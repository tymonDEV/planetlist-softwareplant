import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { ListPlanetsComponent } from './main-view/list-planets/list-planets.component';
import { TasksPlanetsComponent } from './main-view/tasks-planets/tasks-planets.component';
import { ContactComponent } from './contact/contact.component';
import { ListPlanetsDetailsComponent } from './main-view/list-planets/list-planets-details/list-planets-details.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/planets',
        pathMatch: 'full'
    },
    {
        path: 'planets',
        component: ListPlanetsComponent,
    },
    {
        path: 'planets/:id',
        component: ListPlanetsDetailsComponent
    },
    {
        path: 'taskplanets',
        component: TasksPlanetsComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: '**',
        component: ContactComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
