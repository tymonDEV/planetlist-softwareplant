import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transformPlanet'
})
export class TransformPlanetPipe implements PipeTransform {

  transform(value: any, args: string = ''): any {
    return value.charAt(0).toUpperCase() + value.slice(1) + args;
  }

}
