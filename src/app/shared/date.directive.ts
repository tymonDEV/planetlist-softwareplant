import { Directive, HostListener, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appDate]'
})
export class DateDirective {

    @Input()
    private date: string;
    private span;

    constructor(private el: ElementRef, private renderer: Renderer2) {
        this.span = this.renderer.createElement('span');

    }

    @HostListener('mouseenter')
    mouseenter(evenetDate: Event) {
        this.span.innerHTML = this.date;
        this.span.classList.add('badge', 'badge-primary', 'badge-pill', 'float-right');
        this.renderer.appendChild(this.el.nativeElement, this.span);
    }

    @HostListener('mouseleave')
    mouseleave(evenetDate: Event) {
        this.renderer.removeChild(this.el.nativeElement, this.span);
    }

}
