import { Pipe, PipeTransform } from '@angular/core';
import { Planet } from '../models/planet';

@Pipe({
  name: 'sortPlanet'
})
export class SortPlanetPipe implements PipeTransform {

  transform(value: Array<Planet>, args?: any): any {
    return value.sort((a, b) => {
        if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
        } else {
            return -1;
        }
    });
  }

}
