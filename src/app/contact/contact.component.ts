import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'contact-me',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {

    socials: Socials[] = [
        { name: 'Linkedin', url: 'https://www.linkedin.com/in/%C5%82ukasz-tymi%C5%84ski-221460137/', img: 'assets/images/in.png' },
        { name: 'Github', url: 'https://github.com/lukasztyminski', img: 'assets/images/github.png' },
        { name: 'Bitbucket', url: 'https://bitbucket.org/tymonDEV', img: 'assets/images/bit.png' },
    ];

    constructor() { }

    ngOnInit() {
    }

}

interface Socials {
    name: string;
    url: string;
    img: string;
}
