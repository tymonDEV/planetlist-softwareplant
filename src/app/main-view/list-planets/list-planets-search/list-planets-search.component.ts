import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../services/http.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Planet } from '../../../models/planet';
import { serializePath } from '@angular/router/src/url_tree';

@Component({
    selector: 'list-planets-search',
    templateUrl: './list-planets-search.component.html',
    styleUrls: ['./list-planets-search.component.scss']
})
export class ListPlanetsSearchComponent implements OnInit {

    contactForm: FormGroup;

    searchResults: Array<Planet>;

    count: number;

    constructor(private httpService: HttpService) {
    }

    ngOnInit() {
        this.contactForm = new FormGroup({
            search: new FormControl(null, Validators.required)
        });
    }

    onSubmit() {
        const name = this.contactForm.value.search;
        this.httpService.getPlanetsByQuery(name).subscribe(search => {
            this.searchResults = search.results;
            this.count = search.results.length;
        });
    }

}
