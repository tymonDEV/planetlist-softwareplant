import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Planet } from '../../models/planet';
import { Planets } from '../../models/planets';

@Injectable()
export class ListPlanetsService {

    private planetListsObs = new BehaviorSubject<Planets>({});

    constructor(private httpService: HttpService) {
        this.httpService.getPlanets()
            .subscribe((planets: Planets) => {
                this.planetListsObs.next(planets);
            }, error => {
                console.log(error);
            });
    }

    getPlanetListsObs(): Observable<Planets> {
        return this.planetListsObs.asObservable();
    }

}
