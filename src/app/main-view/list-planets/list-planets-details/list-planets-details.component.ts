import { Component, OnInit } from '@angular/core';
import { Planet } from '../../../models/planet';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../../services/http.service';
import { Observable } from 'rxjs/Observable';
import { ListPlanetsService } from '../list-planets.service';
import { Location } from '@angular/common';

@Component({
    selector: 'list-planets-details',
    templateUrl: './list-planets-details.component.html',
    styleUrls: ['./list-planets-details.component.scss']
})
export class ListPlanetsDetailsComponent implements OnInit {

    selected = null;

    planet: Planet;
    constructor(private httpService: HttpService, private route: ActivatedRoute, private location: Location) {
    }

    ngOnInit() {
        this.getPlanet();
    }

    getPlanet(): void {
        this.route.paramMap.subscribe((param: Params) => {
            this.httpService.getPlanet(param.get('id'))
                .subscribe((planet: Planet) => {
                    this.planet = planet;
            });
        });
    }

    goBack(): void {
        this.location.back();
    }

}
