import { Component, OnInit, Input } from '@angular/core';
import { Planet } from '../../models/planet';
import { ListPlanetsService } from './list-planets.service';
import { Planets } from '../../models/planets';
import { HttpService } from '../../services/http.service';
import { Location } from '@angular/common';

@Component({
    selector: 'list-planets',
    templateUrl: './list-planets.component.html',
    styleUrls: ['./list-planets.component.scss']
})
export class ListPlanetsComponent implements OnInit {

    page: number = 1;
    pages: Array<number>;
    planetLists: Array<Planet> = [];
    previous: null | string;
    next: null | string;

    constructor(private listPlanetsService: ListPlanetsService, private httpService: HttpService, private location: Location) {
    }

    ngOnInit() {
        this.renderListPlanets();
    }

    renderListPlanets() {
        this.httpService.getPlanetsByPage(this.page)
            .subscribe((planets: Planets) => {
                this.planetLists = planets.results;
                this.pages = new Array((planets.count - 1) / 10 + 1);
                this.previous = planets.previous;
                this.next = planets.next;
            });
    }

    setPage(i, event: any) {
        event.preventDefault();
        this.page = i + 1;
        this.renderListPlanets();
    }

    goBackPage(i, event: any): void {
        event.preventDefault();
        this.page = this.page + (i - 1);
        this.renderListPlanets();
    }

    goNextPage(i, event: any): void {
        event.preventDefault();
        this.page = this.page - (i - 1);
        this.renderListPlanets();
    }

}
