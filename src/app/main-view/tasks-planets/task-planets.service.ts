import { Injectable } from '@angular/core';
import { Planet } from '../../models/planet';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../services/http.service';

@Injectable()
export class TaskPlanetsService {

    private planetListsObs = new BehaviorSubject<Array<Planet>>([]);

    constructor(private httpService: HttpService) {
        this.httpService.getPlanetsMdb()
            .subscribe(planets => {
                this.planetListsObs.next(planets);
            });
    }

    add(planet: Planet) {
        const listPlanets = this.planetListsObs.getValue();
        listPlanets.push(planet);
        this.planetListsObs.next(listPlanets);
    }

    remove(planet: Planet) {
        const listPlanets = this.planetListsObs.getValue().filter(old => old !== planet);
        this.planetListsObs.next(listPlanets);
    }

    done(planet: Planet) {
        planet.edited = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();
        planet.isDone = true;
        const listPlanet = this.planetListsObs.getValue();
        this.planetListsObs.next(listPlanet);
    }

    getPlanetListsObs(): Observable<Array<Planet>> {
        return this.planetListsObs.asObservable();
    }

    savePlanetsInMdb() {
        this.httpService.savePlanetsMdb(this.planetListsObs.getValue());
    }
}
