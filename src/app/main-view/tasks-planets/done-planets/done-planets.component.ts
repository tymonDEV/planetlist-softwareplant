import { Component, OnInit } from '@angular/core';
import { TaskPlanetsService } from '../task-planets.service';
import { Planet } from '../../../models/planet';

@Component({
    selector: 'done-planets',
    templateUrl: './done-planets.component.html',
    styleUrls: ['./done-planets.component.scss']
})
export class DonePlanetsComponent implements OnInit {

    planetsDone: Array<Planet> = [];

    constructor(private taskPlanetsService: TaskPlanetsService) {
        this.taskPlanetsService.getPlanetListsObs().subscribe((planets: Array<Planet>) => {
            this.planetsDone = planets
                .filter(planet => planet.isDone === true);
        });
    }

    ngOnInit() {
    }

}
