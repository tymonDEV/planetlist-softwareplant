import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TaskPlanetsService } from '../task-planets.service';
import { Planet } from '../../../models/planet';

@Component({
    selector: 'add-planet',
    templateUrl: './add-planet.component.html',
    styleUrls: ['./add-planet.component.scss']
})
export class AddPlanetComponent implements OnInit {

    newPlanet: string;

    constructor(private taskPlanetsService: TaskPlanetsService) {

    }

    ngOnInit() {
    }

    add() {
        const planet: Planet = ({
            name: this.newPlanet, created: new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString(),
            isDone: false
        });
        this.taskPlanetsService.add(planet);
        this.newPlanet = '';
    }

}
