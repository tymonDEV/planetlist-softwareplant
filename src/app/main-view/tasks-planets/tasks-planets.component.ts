import { Component, OnInit } from '@angular/core';
import { TaskPlanetsService } from './task-planets.service';

@Component({
    selector: 'tasks-planets',
    templateUrl: './tasks-planets.component.html',
    styleUrls: ['./tasks-planets.component.scss']
})
export class TasksPlanetsComponent  {

    constructor(private taskPlanetsService: TaskPlanetsService) {}

    savePlanetsInMdb() {
        this.taskPlanetsService.savePlanetsInMdb();
    }

}
