import { Component, OnInit, EventEmitter } from '@angular/core';
import { TaskPlanetsService } from '../task-planets.service';
import { Planet } from '../../../models/planet';
import { HttpService } from '../../../services/http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'todo-planet',
    templateUrl: './todo-planet.component.html',
    styleUrls: ['./todo-planet.component.scss']
})
export class TodoPlanetComponent implements OnInit {

    planetLists: Array<Planet> = [];

    constructor(private taskPlanetsService: TaskPlanetsService, private httpService: HttpService) {
        this.taskPlanetsService.getPlanetListsObs().subscribe((planets: Array<Planet>) => {
            this.planetLists = planets
                .filter(planet => planet.isDone === false);
        });
    }

    ngOnInit() {
    }

    remove(planet: Planet) {
        this.taskPlanetsService.remove(planet);
    }

    done(planet: Planet) {
        this.taskPlanetsService.done(planet);
    }

    getColor(): string {
        return this.planetLists.length >= 5 ? 'red' : '#1cc81c';
    }

}
