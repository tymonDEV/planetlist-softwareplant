import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Planet } from '../models/planet';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import { Planets } from '../models/planets';

@Injectable()
export class HttpService {

    readonly URL_DB = 'https://api.mlab.com/api/1/databases/angulartodo/collections/planets';
    readonly param = new HttpParams().set('apiKey', 'p1Gr3IQ4il1QTK_T9QXspzBZufBlFjqA');

    readonly URL_API = 'https://swapi.co/api/planets/';

    constructor(private http: HttpClient) {}

    // Download all planets
    getPlanets(): Observable<Planets> {
        return this.http.get<Planets>(this.URL_API);
    }

    getPlanetsMdb(): Observable<Array<Planet>> {
        return this.http.get<Array<Planet>>(this.URL_DB, {params: this.param});
    }

    // Download one planets giving id
    getPlanet(id: string): Observable<Planet> {
        return this.http.get<Planet>(this.URL_API + id);
    }

    // Download array with Planets for paramter ?page
    getPlanetsByPage(page: number): Observable<Planets> {
        const parm = new HttpParams().set('page', page + '');
        return this.http.get<Planets>(this.URL_API, { params: parm });
    }

    getPlanetsByQuery(search: string): Observable<Planets> {
        const parm = new HttpParams().set('search', search + '');
        return this.http.get<Planets>(this.URL_API, { params: parm });
    }

    // Adding new Planet
    savePlanetsMdb(planets: Array<Planet>) {
        return this.http.put(this.URL_DB, planets, {params: this.param})
        .subscribe(data => {
            console.log(data);
        });
    }

    // Update Planet
    updatePlanet(planet: Planet): Observable<Planet> {
        return this.http.put<Planet>(this.URL_DB + planet._id, planet);
    }

    // Delete Planet
    deletePlanet(id: number): Observable<Planet> {
        return this.http.delete<Planet>(this.URL_DB + id);
    }

    // Update value Planet
    changePlanet(planet: Planet): Observable<Planet> {
        return this.http.patch<Planet>(this.URL_DB + planet._id, planet);
    }
}
