export interface Planet {
    _id?: { $oid: string };
    id?: number;
    name: string;
    created: string;
    climate?: string;
    diameter?: string;
    population?: string;
    edited?: string;
    search?: string;
    page?: string;
    url?: string;
    residents?: Array<string>;
    films?: Array<string>;
    isDone?: boolean;
}
