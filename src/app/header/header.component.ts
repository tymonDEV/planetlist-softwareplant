import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'header-top',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    logoTitle: string = 'Planets';
    logoUrl: string = 'assets/images/pl.svg';


    constructor() { }

    ngOnInit() {
    }

}
