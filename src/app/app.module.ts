import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ListPlanetsComponent } from './main-view/list-planets/list-planets.component';
import { TasksPlanetsComponent } from './main-view/tasks-planets/tasks-planets.component';
import { DonePlanetsComponent } from './main-view/tasks-planets/done-planets/done-planets.component';
import { AddPlanetComponent } from './main-view/tasks-planets/add-planet/add-planet.component';
import { TodoPlanetComponent } from './main-view/tasks-planets/todo-planet/todo-planet.component';
import { TaskPlanetsService } from './main-view/tasks-planets/task-planets.service';
import { CheckedDirective } from './shared/checked.directive';
import { DateDirective } from './shared/date.directive';
import { TransformPlanetPipe } from './shared/transform-planet.pipe';
import { SortPlanetPipe } from './shared/sort-planet.pipe';
import { HttpService } from './services/http.service';
// import { AuthInterceptor } from './services/auth.interceptor';
import { ContactComponent } from './contact/contact.component';
import { ListPlanetsDetailsComponent } from './main-view/list-planets/list-planets-details/list-planets-details.component';
import { ListPlanetsService } from './main-view/list-planets/list-planets.service';
import { ListPlanetsSearchComponent } from './main-view/list-planets/list-planets-search/list-planets-search.component';



@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AddPlanetComponent,
        ListPlanetsComponent,
        ListPlanetsDetailsComponent,
        TasksPlanetsComponent,
        DonePlanetsComponent,
        TodoPlanetComponent,
        ContactComponent,
        ListPlanetsSearchComponent,
        CheckedDirective,
        DateDirective,
        TransformPlanetPipe,
        SortPlanetPipe
    ],
    imports: [
        BrowserModule,
        NgxPaginationModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [HttpService, TaskPlanetsService, ListPlanetsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
